# inlogic_test_app

It's a small task (Flutter application) for INLOGIC IT Solutions.

- Make an app containing dummy list of objects having images videos simple texts
- Making a screen shot of any view any time
- Open detail of video having video player
- Detail of image having big image view and dummy detail
- Put some animation after splash
- Search with title
- Json parsing

And we use below api for the data:
https://covid19.inlogic.ae/WebApi/api/External/GetIosRandomData
