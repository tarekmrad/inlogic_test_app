import 'package:shared_preferences/shared_preferences.dart';

class PrefsHelper {
  Future<SharedPreferences> _getPrefs() async {
    return await SharedPreferences.getInstance();
  }

  Future<bool> clearData() async {
    return (await _getPrefs()).clear();
  }

  Future<Map<String, dynamic>> getAllDate() async {
    final keys = (await _getPrefs()).getKeys();
    final prefsMap = Map<String, dynamic>();
    for (String key in keys) {
      prefsMap[key] = (await _getPrefs()).get(key);
    }
    return prefsMap;
  }

}
