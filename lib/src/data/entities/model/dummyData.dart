library dummyData;

import 'package:json_annotation/json_annotation.dart';

part 'dummyData.g.dart';

@JsonSerializable(nullable: true)
class DummyData {
  int Id;
  int Type;
  String Path;
  String Name;

  DummyData._();

  DummyData({this.Id});

  factory DummyData.fromJson(Map<String, dynamic> json) => _$DummyDataFromJson(json);
}
