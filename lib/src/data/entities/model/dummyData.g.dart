// GENERATED CODE - DO NOT MODIFY BY HAND

part of dummyData;

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DummyData _$DummyDataFromJson(Map<String, dynamic> json) {
  return DummyData(
    Id: json['Id'] as int,
  )
    ..Type = json['Type'] as int
    ..Path = json['Path'] as String
    ..Name = json['Name'] as String;
}

Map<String, dynamic> _$DummyDataToJson(DummyData instance) => <String, dynamic>{
      'Id': instance.Id,
      'Type': instance.Type,
      'Path': instance.Path,
      'Name': instance.Name,
    };
