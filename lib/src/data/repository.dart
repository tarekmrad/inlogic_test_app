import 'package:dio/dio.dart';
import 'package:inlogic_test_app/src/data/http_helper.dart';
import 'package:inlogic_test_app/src/data/prefs_helper.dart';

class Repository {
  final HttpHelper _httpHelper;
  final PrefsHelper _prefsHelper;

  Repository(this._httpHelper, this._prefsHelper);

  ///////////////////////////// Shared Preferences /////////////////////////////

  Future<Map<String, dynamic>> getAllDate() => _prefsHelper.getAllDate();

  Future<bool> clearUserInfo() => _prefsHelper.clearData();

  ///////////////////////////////// Requests ///////////////////////////////////

  Future<Response> getHome() => _httpHelper.getHome();
}
