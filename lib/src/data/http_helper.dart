import 'dart:async';
import 'dart:io';

import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';
import 'package:inlogic_test_app/src/utils/constants.dart';

class HttpHelper {
  final Dio dio;

  HttpHelper(this.dio) {
    dio.interceptors.add(LogInterceptor(
      requestBody: true,
      responseBody: true,
    ));

    (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (HttpClient client) {
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      return client;
    };
  }


  Future<Response> getHome() async {

    Response response = await dio.get(
      BASE_URL_API + "External/GetIosRandomData",
    );
    return response;
  }

}
