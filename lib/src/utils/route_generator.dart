import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:inlogic_test_app/src/srceens/imageDetails/image_details.dart';
import 'package:inlogic_test_app/src/srceens/main/main.dart';
import 'package:inlogic_test_app/src/srceens/splash/splash.dart';
import 'package:inlogic_test_app/src/srceens/videoPlayer/video_player.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final Map args = settings.arguments as Map;

    switch (settings.name) {
      case RouteNames.SPLASH_SCREEN:
        return MaterialPageRoute(
          builder: (_) => Splash(),
        );

      case RouteNames.MAIN_SCREEN:
        return MaterialPageRoute(
          builder: (_) => Main(),
        );


      case RouteNames.IMAGE_SCREEN:
        return MaterialPageRoute(
          builder: (_) => ImageDetailsScreen(args['data']),
        );

      case RouteNames.VIDEO_SCREEN:
        return MaterialPageRoute(
          builder: (_) => VideoPlayerScreen(args['video']),
        );


//      case RouteNames.LOGIN_SCREEN:
//        if (args is Map)
//          return MaterialPageRoute(
//            builder: (_) => LOGIN_SCREEN(
//              args['isConnected'],
//            ),
//          );
//        return _errorRoute();

      default:
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          title: Text('Error'),
        ),
        body: Center(
          child: Text('ERROR'),
        ),
      );
    });
  }
}

class RouteNames {
  static const SPLASH_SCREEN = '/';
  static const MAIN_SCREEN = '/main_screen';
  static const IMAGE_SCREEN = '/image_screen';
  static const VIDEO_SCREEN = '/video_screen';

}
