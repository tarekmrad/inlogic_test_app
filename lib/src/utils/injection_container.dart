import 'package:dio/dio.dart';
import 'package:inlogic_test_app/src/data/http_helper.dart';
import 'package:inlogic_test_app/src/data/prefs_helper.dart';
import 'package:inlogic_test_app/src/data/repository.dart';
import 'package:inlogic_test_app/src/srceens/imageDetails/bloc.dart';
import 'package:inlogic_test_app/src/srceens/main/bloc.dart';
import 'package:inlogic_test_app/src/srceens/splash/bloc.dart';
import 'package:inlogic_test_app/src/srceens/videoPlayer/bloc.dart';
import 'package:kiwi/kiwi.dart' as Kiwi;

void iniKiwi() {
  Kiwi.KiwiContainer()
    ..registerInstance(Dio(
      BaseOptions(

        connectTimeout: 60000,
        responseType: ResponseType.plain,
        headers: {
          'accept': 'text/plain',
          // 'Content-Type': 'multipart/form-data'
          'content-type': 'application/json'
        },
      ),

    ))
    ..registerSingleton((c) => HttpHelper(c.resolve()))
    ..registerSingleton((c) => PrefsHelper())
    ..registerSingleton((c) => Repository(c.resolve(), c.resolve()))
    ..registerFactory((c) => SplashBloc(c.resolve()))
    ..registerFactory((c) => MainBloc(c.resolve()))
    ..registerFactory((c) => ImageDetailsBloc(c.resolve()))
    ..registerFactory((c) => VideoPlayerBloc(c.resolve()));

}
