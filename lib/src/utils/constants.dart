const BASE_URL = 'https://covid19.inlogic.ae/WebApi';
const BASE_URL_API = BASE_URL + '/api/';

const APPLICATION_LANGUAGE = 'application_language';

// Status codes
const RESPONSE_OK = 200;
const RESPONSE_CREATED = 204;
const RESPONSE_UNAUTHORIZED = 401;
const RESPONSE_INTERNAL_ERROR = 500;

// Exception types
const NO_ERROR = 0;
const NETWORK_EXCEPTION = 1;
const ERROR_GETTING_DATA = 2;
const DESERIALIZE_EXCEPTION = 3;
const INVALID_INPUT = 4;
