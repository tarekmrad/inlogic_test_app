import 'package:flutter/material.dart';
// import 'package:flutter_svg/flutter_svg.dart';
import 'package:inlogic_test_app/src/utils/colors.dart';

class Images {
  static const Icon play_circle_fill =
  Icon(Icons.play_circle_fill, color: WHITE_COLOR, size: 42);

  //////////////////////////////////////////////////////////////////////////////

  static Image splash_logo = new Image.asset(
    'assets/images/splash_logo.png',
  );


  static Image background = new Image.asset(
    'assets/images/place_holder.jpeg',
    fit: BoxFit.cover,
  );

  static AssetImage place_holder = new AssetImage(
    'assets/images/place_holder.jpeg',
  );

}
