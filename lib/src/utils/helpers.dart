import 'constants.dart';

class Helper {
  static String responseMessage(int code) {
    switch (code) {
      case RESPONSE_INTERNAL_ERROR:
        return "RESPONSE_INTERNAL_ERROR";
        break;

      case RESPONSE_UNAUTHORIZED:
        return "RESPONSE_UNAUTHORIZED";
        break;
    }
    return "";
  }
}
