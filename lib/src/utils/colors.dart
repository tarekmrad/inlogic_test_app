import 'package:flutter/material.dart';

const GREY = Color.fromARGB(255, 150, 150, 150);
const LIGHT_GREY = Color.fromARGB(255, 216, 216, 216);
const DARK_GREY = Color.fromARGB(255, 75, 75, 75);

const DARK_GRAY_COLOR = Color(0xff4C4C4C);
const PURPLE_COLOR = Color(0xff672F92);
const BLACK_COLOR = Color(0xff000000);
const BACKGROUND_COLOR = Color(0xFFFCFCFC);
const WHITE_COLOR = Color(0xFFFFFFFF);
const TEXTFIELD_BACKGROUND_COLOR = Color(0xFFF2F2F2);
const LIGHT_TEXT_COLOR = Color(0xFFB0B0B0);

var BACKGROUND_LIGHT_COLOR = Color(0xFFFFFFFF).withOpacity(0.9);
const BUTTON_BACKGROUND_COLOR = Color(0xFFF3F3F5);
const IMAGE_BACKGROUND_COLOR = Color(0xFFF8F8F8);

const TEXT_TITLE_COLOR = Color(0xff707070);
const TEXT_COLOR = Color(0xff676767);
const TEXT_FIELD_BORDER_COLOR = Color(0xffDDDDDD);
const PROFILE_BOX_COLOR = Color(0xFFF2F2F2);
const PROFILE_BOX_BORDER_COLOR = Color(0xffcccccc);
const PRODUCT_BOX_BORDER_COLOR = Color(0xffD3D3D3);
const RED_COLOR = Color(0xffEF0000);
const GREEN_COLOR = Color(0xff019F07);
const BLUE_COLOR = Color(0xff0095D5);
