import 'package:flutter/material.dart';
import 'package:inlogic_test_app/src/utils/colors.dart';

showSnackBarMessage(
  GlobalKey<ScaffoldState> _scaffoldKey,
  String message,
  Color color, {
  Color backgroundColor = DARK_GRAY_COLOR,
  Duration duration = const Duration(milliseconds: 3000),
}) {
  _scaffoldKey.currentState.showSnackBar(
    new SnackBar(
      duration: duration,
      content: Text(
        message,
        style: TextStyle(
          fontSize: 15,
          color: color,
          fontWeight: FontWeight.w600,
        ),
      ),
      backgroundColor: backgroundColor,
    ),
  );
}

showMessageDialog(BuildContext context, String message) {
  showDialog(
    context: context,
    barrierDismissible: true,
    builder: (BuildContext context) {
      return AlertDialog(
        content: Text(message),
        actions: [
          FlatButton(
            child: Text("Ok"),
            onPressed: () {},
          ),
        ],
      );
    },
  );
}

showConfirmDialog(BuildContext context, String message, Function onConfirm) {
  showDialog(
    context: context,
    barrierDismissible: true,
    builder: (BuildContext context) {
      return AlertDialog(
        content: Text(
          message,
          style: TextStyle(
            color: DARK_GREY,
            fontSize: 16,
          ),
        ),
        actions: [
          FlatButton(
            child: Text(
              "Cancel",
              style: TextStyle(
                color: GREY,
                fontWeight: FontWeight.w600,
                fontSize: 14,
              ),
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          FlatButton(
            child: Text(
              "Ok",
              style: TextStyle(
                color: DARK_GRAY_COLOR,
                fontWeight: FontWeight.w600,
                fontSize: 14,
              ),
            ),
            onPressed: () {
              onConfirm();
              Navigator.pop(context);
            },
          ),
        ],
      );
    },
  );
}

AlertDialog showLoadingDialog(String title) {
  AlertDialog alertDialog = new AlertDialog(
    content: Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Row(
        children: <Widget>[
          CircularProgressIndicator(
            valueColor: new AlwaysStoppedAnimation<Color>(PURPLE_COLOR),
          ),
          SizedBox(
            width: 30,
          ),
          Text(
            title,
            style: TextStyle(fontSize: 14, color: DARK_GRAY_COLOR),
          )
        ],
      ),
    ),
  );
  return alertDialog;
}
