import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:inlogic_test_app/src/data/entities/model/dummyData.dart';
import 'package:inlogic_test_app/src/utils/app_localization.dart';
import 'package:inlogic_test_app/src/utils/colors.dart';
import 'package:inlogic_test_app/src/utils/images.dart';
import 'package:kiwi/kiwi.dart' as Kiwi;

import 'bloc.dart';

class ImageDetailsScreen extends StatefulWidget {
  DummyData data;

  ImageDetailsScreen(this.data);

  @override
  _ImageDetailsScreenState createState() => _ImageDetailsScreenState(data);
}

class _ImageDetailsScreenState extends State<ImageDetailsScreen> {
  _ImageDetailsScreenState(this.data);

  DummyData data;

  final _bloc = Kiwi.KiwiContainer().resolve<ImageDetailsBloc>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void dispose() {
    _bloc.close();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      cubit: _bloc,
      listener: (context, ImageDetailsState state) {
        if (state is BackToPreviousScreenState) Navigator.of(context).pop();
      },
      child: BlocBuilder(
        cubit: _bloc,
        builder: (context, ImageDetailsState state) {
          return Scaffold(
            key: _scaffoldKey,
            body: CustomScrollView(
              slivers: <Widget>[
                SliverAppBar(
                  leading: IconButton(
                    padding: EdgeInsets.zero,
                    constraints: BoxConstraints(),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    icon: Icon(Icons.arrow_back, color: WHITE_COLOR, size: 28),
                  ),
                  expandedHeight: MediaQuery.of(context).size.height * 0.3,
                  elevation: 20,
                  floating: true,
                  pinned: true,
                  snap: true,
                  backgroundColor: PURPLE_COLOR,
                  flexibleSpace: FlexibleSpaceBar(
                      title: Text("Title",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 16.0,
                          )),
                      background: FadeInImage(
                          fit: BoxFit.cover,
                          placeholder: Images.place_holder,
                          image: NetworkImage(
                              data.Path != null ? data.Path : ""))),
                ),
                SliverFillRemaining(
                  child: Container(
                    width: double.infinity,
                    decoration: BoxDecoration(
                      color: WHITE_COLOR,
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(40.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(height: 10.0),
                          Text(
                            data.Name,
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: BLACK_COLOR,
                              height: 2,
                              fontSize: 20,
                            ),
                          ),
                          SizedBox(height: 10.0),
                        ],
                      ),
                    ),
                  ),
                )
              ],
            ),
          );
        },
      ),
    );
  }

  String getString(String key) => AppLocalizations.of(context).translate(key);
}
