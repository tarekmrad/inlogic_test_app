import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:inlogic_test_app/src/data/repository.dart';

import './bloc.dart';

class ImageDetailsBloc extends Bloc<ImageDetailsEvent, ImageDetailsState> {
  final Repository _repository;

  ImageDetailsBloc(this._repository) : super(null);

  @override
  ImageDetailsState get initialState => InitialImageDetailsState();

  @override
  Stream<ImageDetailsState> mapEventToState(ImageDetailsEvent event) async* {
    if (event is BackArrowClicked) {
      yield BackToPreviousScreenState();
    }
  }
}
