abstract class ImageDetailsEvent {}

class GetUserInfoEvent extends ImageDetailsEvent {}

class BackArrowClicked extends ImageDetailsEvent {}

class SearchIconClicked extends ImageDetailsEvent {}
