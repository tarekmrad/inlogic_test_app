import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:inlogic_test_app/src/data/entities/model/dummyData.dart';
import 'package:inlogic_test_app/src/data/repository.dart';
import 'package:inlogic_test_app/src/utils/constants.dart';
import 'package:inlogic_test_app/src/utils/helpers.dart';

import './bloc.dart';

class MainBloc extends Bloc<MainEvent, MainState> {
  final Repository _repository;
  List<DummyData> list;
  List<DummyData> _searchResult = [];

  MainBloc(this._repository) : super(null);

  @override
  MainState get initialState => InitialMainState();

  @override
  Stream<MainState> mapEventToState(MainEvent event) async* {
    if (event is GetListEvent) {
      yield* mapGetList();
    } else if (event is GetResultSearchEvent) {
      if (event.text.isNotEmpty) {
        _searchResult.clear();
        list.forEach((userDetail) {
          if (userDetail.Name.toLowerCase().contains(event.text.toLowerCase()))
            _searchResult.add(userDetail);
        });
        yield GetAllItemsSucceed(list: _searchResult);
      } else {
        yield GetAllItemsSucceed(list: list);
      }
    }
  }

  Stream<MainState> mapGetList() async* {
    yield GettingAllItems();

    Response objectResponse;

    await _repository
        .getHome()
        .then((result) => objectResponse = result)
        .catchError((error) => print('getHome error = $error'));

    if (objectResponse != null) {
      if (objectResponse.statusCode == RESPONSE_OK) {
        list = (json.decode(objectResponse.data) as List)
            .map((data) => DummyData.fromJson(data))
            .toList();

        if (list != null) {
          yield GetAllItemsSucceed(list: list);
        }
      } else {
        String error = Helper.responseMessage(objectResponse.statusCode);
        if (error.isNotEmpty)
          yield GetAllItemsFailed(message: error);
        else
          yield GetAllItemsFailed();
      }
    }
  }
}
