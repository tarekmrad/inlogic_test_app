
import 'package:inlogic_test_app/src/data/entities/model/dummyData.dart';

abstract class MainState {}

class InitialMainState extends MainState {}

////////////////////////////////////////////////////////////////////////////////

class GettingAllItems extends MainState {}

class GetAllItemsSucceed extends MainState {
  final List<DummyData> list;

  GetAllItemsSucceed({this.list}) : super();
}

class GetAllItemsFailed extends MainState {
  final String message;

  GetAllItemsFailed({this.message});
}