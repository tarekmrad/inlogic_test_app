abstract class MainEvent {}

class GetListEvent extends MainEvent {}

class GetResultSearchEvent extends MainEvent {
  final String text;

  GetResultSearchEvent({
    this.text,
  });
}
