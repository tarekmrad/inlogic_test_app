import 'dart:async';
import 'dart:io';
import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';

// import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:inlogic_test_app/src/data/entities/model/dummyData.dart';
import 'package:inlogic_test_app/src/utils/app_localization.dart';
import 'package:inlogic_test_app/src/utils/colors.dart';
import 'package:inlogic_test_app/src/utils/images.dart';
import 'package:inlogic_test_app/src/utils/route_generator.dart';
import 'package:inlogic_test_app/src/utils/utils.dart';
import 'package:kiwi/kiwi.dart' as Kiwi;
import 'package:permission_handler/permission_handler.dart';
import 'package:screenshot/screenshot.dart';

import 'bloc.dart';

class Main extends StatefulWidget {
  @override
  _MainState createState() => _MainState();
}

class _MainState extends State<Main> {
  final _bloc = Kiwi.KiwiContainer().resolve<MainBloc>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  List<DummyData> list;

  final searchController = TextEditingController();
  String textValue;
  Timer timeHandle;

  File _imageFile;
  ScreenshotController screenshotController = ScreenshotController();

  void textChanged(String val) {
    textValue = val;
    if (timeHandle != null) {
      timeHandle.cancel();
    }
    timeHandle = Timer(Duration(seconds: 1), () {
      _bloc.add(GetResultSearchEvent(text: textValue));
    });
  }

  takeScreenShot() async {
    screenshotController.capture().then((File image) {
      setState(() {
        _imageFile = image;
      });
    }).catchError((onError) {
      print(onError);
    });

    PermissionStatus status = await Permission.storage.request();
    if (status.isGranted) {
      final result = await ImageGallerySaver.saveFile(_imageFile.path);
      showSnackBarMessage(_scaffoldKey, "Capture Done", Colors.green);
    }
  }

  @override
  void initState() {
    super.initState();
    _bloc.add(GetListEvent());
  }

  @override
  void dispose() {
    _bloc.close();
    searchController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      cubit: _bloc,
      listener: (context, MainState state) {
        if (state is GetAllItemsSucceed) {
          list = state.list;
        }
      },
      child: BlocBuilder(
        cubit: _bloc,
        builder: (context, MainState state) {
          return Scaffold(
            key: _scaffoldKey,
            floatingActionButton: FloatingActionButton(
              onPressed: () async {
                takeScreenShot();
              },
              child: Icon(
                Icons.mobile_screen_share_rounded,
              ),
            ),
            body: Screenshot(
              // key: previewContainer,
              controller: screenshotController,
              child: Container(
                height: double.infinity,
                decoration: BoxDecoration(
                  color: BACKGROUND_COLOR,
                ),
                child: Stack(
                  children: <Widget>[
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          decoration: BoxDecoration(
                            color: BACKGROUND_COLOR,
                            borderRadius: new BorderRadius.only(
                              bottomRight: const Radius.circular(30.0),
                              bottomLeft: const Radius.circular(30.0),
                            ),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.2),
                                spreadRadius: 4,
                                blurRadius: 5,
                                offset: Offset(0, 1),
                              ),
                            ],
                          ),
                          child: Column(
                            children: [
                              SizedBox(height: 40.0),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 34, vertical: 10),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          getString('lbl_Hi') + ", User",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              height: 1.25,
                                              color: DARK_GRAY_COLOR,
                                              fontSize: 32.0),
                                        ),
                                        Text(
                                          getString('lbl_do_today'),
                                          style: TextStyle(
                                              height: 1.25,
                                              color: DARK_GRAY_COLOR,
                                              fontSize: 15.0),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                          child: SingleChildScrollView(
                            child: AnimationLimiter(
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children:
                                    AnimationConfiguration.toStaggeredList(
                                  duration: const Duration(milliseconds: 1000),
                                  childAnimationBuilder: (widget) =>
                                      SlideAnimation(
                                    horizontalOffset: 50.0,
                                    child: FadeInAnimation(
                                      child: widget,
                                    ),
                                  ),
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 24, vertical: 36),
                                      child: TextField(
                                        onChanged: textChanged,
                                        textInputAction: TextInputAction.search,
                                        style: TextStyle(
                                            fontWeight: FontWeight.w600,
                                            color: DARK_GRAY_COLOR,
                                            fontSize: 15.0),
                                        controller: searchController,
                                        keyboardType: TextInputType.text,
                                        decoration: InputDecoration(
                                          hintText: "Search",
                                          hintStyle: TextStyle(fontSize: 15),
                                          border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(30),
                                            borderSide: BorderSide(
                                              width: 0,
                                              style: BorderStyle.none,
                                            ),
                                          ),
                                          filled: true,
                                          contentPadding: EdgeInsets.symmetric(
                                              vertical: 0, horizontal: 20),
                                          fillColor: TEXTFIELD_BACKGROUND_COLOR,
                                        ),
                                      ),
                                    ),
                                    list != null
                                        ? list.length != 0
                                            ? Padding(
                                                padding:
                                                    const EdgeInsets.symmetric(
                                                        horizontal: 24),
                                                child:
                                                    getList(),
                                              )
                                            : Padding(
                                                padding: const EdgeInsets.only(
                                                    top: 125),
                                                child: Center(
                                                  child: Text(
                                                    "No Result!",
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                        color: TEXT_TITLE_COLOR,
                                                        fontSize: 20,
                                                        fontWeight:
                                                            FontWeight.w500),
                                                  ),
                                                ),
                                              )
                                        : SizedBox(),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    BlocBuilder(
                      cubit: _bloc,
                      builder: (context, MainState state) {
                        return Opacity(
                          opacity: state is GettingAllItems ? 1.0 : 0,
                          child: Center(
                            child: CircularProgressIndicator(
                              valueColor: new AlwaysStoppedAnimation<Color>(
                                  PURPLE_COLOR),
                            ),
                          ),
                        );
                      },
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  Widget getList() {
    return StaggeredGridView.countBuilder(
      padding: const EdgeInsets.all(0.0),
      primary: false,
      shrinkWrap: true,
      itemCount: list.length,
      physics: NeverScrollableScrollPhysics(),
      staggeredTileBuilder: (int index) =>
          new StaggeredTile.count(2, index.isEven ? 2 : 1),
      mainAxisSpacing: 4.0,
      crossAxisSpacing: 4.0,
      crossAxisCount: 4,
      itemBuilder: (BuildContext context, int index) => Card(
        elevation: 5,
        color: BACKGROUND_LIGHT_COLOR,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        margin: EdgeInsets.all(8.0),
        child: new Container(
          decoration: ShapeDecoration(
            color: BACKGROUND_LIGHT_COLOR,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
          ),
          child: GestureDetector(
            onTap: () {
              if (list[index].Type == 1)
                Navigator.pushNamed(context, RouteNames.IMAGE_SCREEN,
                    arguments: {'data': list[index]});
              else if (list[index].Type == 2)
                Navigator.pushNamed(context, RouteNames.VIDEO_SCREEN,
                    arguments: {'video': list[index].Path});
            },
            child: Stack(
              children: <Widget>[
                Container(
                    width: double.infinity,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10.0),
                      child: FadeInImage(
                          fit: BoxFit.cover,
                          placeholder: Images.place_holder,
                          image: NetworkImage(
                              list[index].Path != null && list[index].Type == 1
                                  ? list[index].Path
                                  : "")),
                    )),
                Container(
                  decoration: ShapeDecoration(
                    color: Colors.black26,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                ),
                Center(child: itemType(list[index])),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget itemType(DummyData item) {
    if (item.Type == 1)
      return Container(
        decoration: BoxDecoration(
          color: BACKGROUND_COLOR.withOpacity(0.5),
          borderRadius: BorderRadius.circular(10),
        ),
        child: Padding(
          padding: const EdgeInsets.all(6.0),
          child: Text(
            getString('lbl_Show'),
            textAlign: TextAlign.center,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 16,
              color: Colors.white,
              height: 1.25,
            ),
          ),
        ),
      );
    else if (item.Type == 2)
      return Images.play_circle_fill;
    else if (item.Type == 3)
      return Text(
        item.Name,
        textAlign: TextAlign.center,
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 18,
          color: Colors.white,
          height: 1.25,
        ),
      );
  }

  String getString(String key) => AppLocalizations.of(context).translate(key);
}
