abstract class SplashState {}

class InitialSplashState extends SplashState {}

class NavigateToLoginScreenState extends SplashState {}

class NavigateToHomeScreenState extends SplashState {}
