import 'package:animated_widgets/animated_widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:inlogic_test_app/src/utils/colors.dart';
import 'package:inlogic_test_app/src/utils/images.dart';
import 'package:inlogic_test_app/src/utils/route_generator.dart';
import 'package:kiwi/kiwi.dart' as Kiwi;

import 'bloc.dart';

class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> with TickerProviderStateMixin {
  final _bloc = Kiwi.KiwiContainer().resolve<SplashBloc>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void dispose() {
    _bloc.close();
    super.dispose();
  }

  @override
  void initState() {
    Future.delayed(Duration(milliseconds: 2500), () {
      _bloc.add(NavigateToStartScreenEvent());
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      cubit: _bloc,
      listener: (context, SplashState state) {
        if (state is NavigateToHomeScreenState)
          Navigator.pushReplacementNamed(context, RouteNames.MAIN_SCREEN);
      },
      child: BlocBuilder(
          cubit: _bloc,
          builder: (context, SplashState state) {
            return Scaffold(
              key: _scaffoldKey,
              backgroundColor: WHITE_COLOR,
              body: Center(
                child: TranslationAnimatedWidget(
                  values: [
                    Offset(0, 200), // disabled value value
                    Offset(0, 250), //intermediate value
                    Offset(0, 0) //enabled value
                  ],
                  child: Container(
                    child: Images.splash_logo,
                    height: 200.0,
                    width: 200.0,
                  ),
                ),

              ),
            );
          }),
    );
  }
}
