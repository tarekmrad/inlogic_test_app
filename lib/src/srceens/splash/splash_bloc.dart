import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:inlogic_test_app/src/data/repository.dart';

import './bloc.dart';

class SplashBloc extends Bloc<SplashEvent, SplashState> {
  final Repository _repository;

  SplashBloc(this._repository) : super(null);

  @override
  SplashState get initialState => InitialSplashState();

  @override
  Stream<SplashState> mapEventToState(SplashEvent event) async* {
    if (event is NavigateToStartScreenEvent) {
        yield NavigateToHomeScreenState();
    }
  }
}
