abstract class VideoPlayerState {}

class InitialVideoPlayerState extends VideoPlayerState {}

class PauseVideoPlayerState extends VideoPlayerState {}

class PlayVideoPlayerState extends VideoPlayerState {}
