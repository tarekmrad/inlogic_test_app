abstract class VideoPlayerEvent {}

class NavigateToStartScreenEvent extends VideoPlayerEvent {}

class ChangePlayerState extends VideoPlayerEvent {
  final bool isPlaying;

  ChangePlayerState({
    this.isPlaying,
  });
}
