import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:inlogic_test_app/src/data/repository.dart';

import './bloc.dart';

class VideoPlayerBloc extends Bloc<VideoPlayerEvent, VideoPlayerState> {
  final Repository _repository;

  VideoPlayerBloc(this._repository) : super(null);

  @override
  VideoPlayerState get initialState => InitialVideoPlayerState();

  @override
  Stream<VideoPlayerState> mapEventToState(VideoPlayerEvent event) async* {
    if (event is ChangePlayerState) {
      if(event.isPlaying)
        yield PauseVideoPlayerState();
      else
        yield PlayVideoPlayerState();

    }
  }
}
