import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:inlogic_test_app/src/utils/colors.dart';
import 'package:kiwi/kiwi.dart' as Kiwi;
import 'package:video_player/video_player.dart';

import 'bloc.dart';

class VideoPlayerScreen extends StatefulWidget {
  String video;

  VideoPlayerScreen(this.video);

  @override
  _VideoPlayerScreenState createState() => _VideoPlayerScreenState(video);
}

class _VideoPlayerScreenState extends State<VideoPlayerScreen> {
  _VideoPlayerScreenState(this.video);

  String video;

  final _bloc = Kiwi.KiwiContainer().resolve<VideoPlayerBloc>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  VideoPlayerController _controller;

  @override
  void dispose() {
    _bloc.close();
    _controller.dispose();
    super.dispose();
  }

  @override
  void initState() {
    _controller = VideoPlayerController.network(
      // video,
      'https://flutter.github.io/assets-for-api-docs/assets/videos/bee.mp4',
      videoPlayerOptions: VideoPlayerOptions(mixWithOthers: true),
    );

    _controller.addListener(() {
      setState(() {});
    });
    _controller.setLooping(true);
    _controller.initialize();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      cubit: _bloc,
      listener: (context, VideoPlayerState state) {
        if (state is PauseVideoPlayerState)
          _controller.pause();
        else if (state is PlayVideoPlayerState) _controller.play();
      },
      child: BlocBuilder(
          cubit: _bloc,
          builder: (context, VideoPlayerState state) {
            return Scaffold(
              key: _scaffoldKey,
              backgroundColor: WHITE_COLOR,
              floatingActionButton: FloatingActionButton(
                onPressed: () {
                  _bloc.add(ChangePlayerState(
                      isPlaying: _controller.value.isPlaying));
                },
                child: Icon(
                  _controller.value.isPlaying ? Icons.pause : Icons.play_arrow,
                ),
              ),
              body: Center(
                child: _controller.value.initialized
                    ? AspectRatio(
                        aspectRatio: _controller.value.aspectRatio,
                        child: VideoPlayer(_controller),
                      )
                    : Container(),
              ),
            );
          }),
    );
  }
}
