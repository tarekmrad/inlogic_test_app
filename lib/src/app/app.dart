import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:inlogic_test_app/src/utils/app_localization.dart';
import 'package:inlogic_test_app/src/utils/colors.dart';
import 'package:inlogic_test_app/src/utils/constants.dart';
import 'package:inlogic_test_app/src/utils/cupertino_swipe_backobserver.dart';
import 'package:inlogic_test_app/src/utils/fallback_cupertino_localisations_delegate.dart';
import 'package:inlogic_test_app/src/utils/no_splash_factory.dart';
import 'package:inlogic_test_app/src/utils/route_generator.dart';
import 'package:shared_preferences/shared_preferences.dart';

class App extends StatefulWidget {

  @override
  _AppState createState() => _AppState();

  static void setLocale(BuildContext context, Locale newLocale) {
    _AppState state = context.ancestorStateOfType(TypeMatcher<_AppState>());

    WidgetsBinding.instance.addPostFrameCallback((_) => state.setState(() {
          state.locale = newLocale;
        }));
  }

}

class _AppState extends State<App> {
  Locale locale;

  @override
  void initState() {
    super.initState();

    this._fetchLocale().then((locale) {
      setState(() {
        this.locale = locale;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return this.locale == null
        ? CircularProgressIndicator()
        : MaterialApp(
            locale: locale,
            debugShowCheckedModeBanner: false,
            theme: ThemeData(
              pageTransitionsTheme: PageTransitionsTheme(builders: {
                TargetPlatform.android: CupertinoPageTransitionsBuilder(),
                TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
              }),
              splashFactory: NoSplashFactory(),
              primaryColor: DARK_GRAY_COLOR,
              cursorColor: PURPLE_COLOR,
              textSelectionHandleColor: DARK_GRAY_COLOR,
              accentColor: PURPLE_COLOR,
              cupertinoOverrideTheme: CupertinoThemeData(
                primaryColor: DARK_GRAY_COLOR,
              ),
            ),
            navigatorObservers: <NavigatorObserver>[
              CupertinoSwipeBackObserver(),
            ],
            supportedLocales: [
              Locale('en'),
              Locale('ar'),
            ],
            localizationsDelegates: [
              AppLocalizations.delegate,
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
              FallbackCupertinoLocalisationsDelegate(),
            ],
            localeResolutionCallback: (locale, supportedLocales) {
              for (var supportedLocale in supportedLocales) {
                if (locale != null &&
                    supportedLocale.languageCode == locale.languageCode) {
                  return supportedLocale;
                }
              }
              return supportedLocales.first;
            },
            onGenerateRoute: RouteGenerator.generateRoute,
            initialRoute: RouteNames.SPLASH_SCREEN,
          );
  }

  _fetchLocale() async {
    var prefs = await SharedPreferences.getInstance();

    if (prefs.getString(APPLICATION_LANGUAGE) == null) {
      return Locale('en');
    }

    String currentLang = prefs.getString(APPLICATION_LANGUAGE);

    return Locale(currentLang);
  }
}
