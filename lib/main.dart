import 'package:flutter/material.dart';
import 'package:inlogic_test_app/src/app/app.dart';
import 'package:inlogic_test_app/src/utils/injection_container.dart';

void main() async {
  // WidgetsFlutterBinding.ensureInitialized();
  // SharedPreferences.setMockInitialValue/s({});
  // await Firebase.initializeApp();
  iniKiwi();
  runApp(App());
}
